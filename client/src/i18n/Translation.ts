export interface Translation {
  navbar: {
    upload: string;
    wall: string;
    fullscreen: string;
  };
  upload: {
    title: string;
    step: string;
    subtitle: {
      selectImages: string;
      checkImages: string;
      uploadInProgress: string;
      uploadError: string;
      uploadDone: string;
    };
    button: {
      remove: string;
      upload: string;
      back: string;
    };
  };
}
