import { Translation } from '@/i18n/Translation';

class I18n {
  private translations: { [lang: string]: Translation } = {
    DE: require('./DE.json') as Translation,
    EN: require('./EN.json') as Translation,
  };

  public get(): Translation {
    const currentLang = localStorage.getItem('lang');
    if (currentLang != null && this.translations.hasOwnProperty(currentLang)) {
      return this.translations[currentLang];
    }
    return this.translations.DE;
  }
}

export const I18N = new I18n();
