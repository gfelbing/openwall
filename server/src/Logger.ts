export enum Level {
  DEBUG = 0,
  INFO = 1,
  WARN = 2,
  ERROR = 3
}

interface Appender {
  write: (name: String, level: Level, msg: String) => void;
}

class ConsoleAppender implements Appender {
  public write(name: String, level: Level, msg: String) {
    console.log(`[${new Date().toISOString()}][${name}][${level}] ${msg}`)
  }
}

export class Logger {
  private name: String;
  private level: Level;
  private appender: Appender;

  constructor(name: String, level: Level, appender: Appender = new ConsoleAppender()) {
    this.name = name;
    this.level = level;
    this.appender = appender;
  }

  public debug(msg: String) {
    this.log(Level.DEBUG, msg);
  }

  public info(msg: String) {
    this.log(Level.INFO, msg);
  }

  public warn(msg: String) {
    this.log(Level.WARN, msg);
  }

  public error(msg: String) {
    this.log(Level.ERROR, msg);
  }

  public log(lvl: Level, msg: String) {
    if (lvl >= this.level) {
      this.appender.write(this.name, this.level, msg);
    }
  }
}
