import * as fs from 'fs'
import { Level, Logger } from './Logger';

export interface Configuration {
  port: string,
  webdav: {
    url: string,
    user: string,
    password: string,
  },
  log: keyof typeof Level,
}

export function loadConfiguration() {
  // Init with default
  let config: Configuration = {
    port: '8080',
    webdav: {
      url: 'UNSET',
      user: 'UNSET',
      password: 'UNSET',
    },
    log: 'INFO',
  };
  // Load from file
  if (process.env.CONFIG_FILE) {
    const configFile: Configuration = JSON.parse(fs.readFileSync(process.env.CONFIG_FILE).toString());
    config = {
      ...config,
      ...configFile
    };
  }
  // Override with env
  if (process.env.PORT) {
    config.port = process.env.PORT;
  }
  if (process.env.WEBDAV_URL) {
    config.webdav.url = process.env.WEBDAV_URL;
  }
  if (process.env.WEBDAV_USER) {
    config.webdav.user = process.env.WEBDAV_USER;
  }
  if (process.env.WEBDAV_PASSWORD) {
    config.webdav.password = process.env.WEBDAV_PASSWORD;
  }
  if (process.env.LOG) {
    config.log = process.env.LOG.toUpperCase() as keyof typeof Level;
  }
  const logLevel : keyof typeof Level = config.log;
  const log = new Logger(
    "Server",
    Level[config.log]
  );
  log.info(`Loaded config: ${JSON.stringify(config)}`);
  return config;
}



