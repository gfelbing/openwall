import * as express from 'express'

import * as ws from 'ws'
import * as http from 'http';
import * as FileUpload from 'express-fileupload';
import { UploadedFile } from 'express-fileupload';
import { Level, Logger } from './Logger';
import { v4 as uuid } from 'uuid';
import Signals = NodeJS.Signals;
import { Configuration, loadConfiguration } from './Config';
import * as dateFormat from 'dateformat'

const config : Configuration = loadConfiguration();

interface ReadDirError {
  message: string
}

const app = express();
app.use(FileUpload());
const wfs = require("webdav-fs")(
  config.webdav.url,
  config.webdav.user,
  config.webdav.password,
);

const log = new Logger(
  "Server",
  Level[config.log]
);

const server = http.createServer(app);
const wss = new ws.Server({server, path: '/ws'});
const mimetypes = new Set<string>(["image/jpeg", "image/png"]);

['SIGINT', 'SIGTERM'].forEach((signal: Signals) => {
  process.on(signal, function () {
    log.info('Shutting down.');
    server.close();
    process.exit(0)
  });
});

app.use(express.static("static"));

app.get('/api/v1/image/:image', (req, res) => {
  const image = req.params.image;
  if (!files.has(image)) {
    res.status(404);
    res.send();
    log.info(`Image ${image} not found.`);
    return;
  }
  const serverImage = files.get(image);
  const ifModifiedSince = req.headers["if-modified-since"] as string;
  if (ifModifiedSince && new Date(ifModifiedSince).getTime() <= serverImage.mtime) {
    res.status(304);
    res.send();
    log.debug(`Image ${image} unchanged.`);
    return;
  }
  const lastModified = new Date();
  lastModified.setTime(serverImage.mtime);
  res.set('Last-Modified', lastModified.toUTCString());
  res.set('Cache-Control', 'private, max-age=31557600');
  wfs.createReadStream(`/${image}`).pipe(res);
  log.debug(`Image ${image} transmitted.`);
});

app.post('/api/v1/image/:image', (req, res) => {
  if (!req.files) {
    log.info("Empty image upload.");
    return res.status(400).send("No files uploaded.");
  }
  const file = req.files.file as UploadedFile;
  let mimetype = file.mimetype;
  if (!mimetypes.has(mimetype)) {
    log.info("Invalid mimetype upload.");
    return res.status(400).send(`Invalid mimetype: '${mimetype}'.`)
  }
  const time = dateFormat(new Date(), "yyyy-mm-dd'T'HH-MM-ss");
  const image = `${time}_${uuid()}_${req.params.image}`;
  wfs.writeFile(`/${image}`, file.data, (err: any) => {
    if (!err) {
      log.debug(`${image} uploaded.`);
      return res.status(202).send({state: "ok"});
    } else {
      log.warn(`Error during upload of ${image}: ${err}`);
      return res.status(500).send({state: "error"});
    }
  });
});

const broadcast = (message: string) => {
  wss.clients.forEach((client) => {
    client.send(message);
  })
};

interface Stat {
  mtime: number,
  name: string,
  size: number
}

const files = new Map<string, Stat>();

const setToJSON = (set: Map<string, Stat>): string => {
  return JSON.stringify([...set.values()]
                          .sort((s1, s2) => s2.mtime - s1.mtime)
                          .map(s => s.name)
                          .slice(0, 50));
};

setInterval(() => {
  wfs.readdir("/", function (err: ReadDirError, contents: Stat[]) {
    if (!err) {
      const images = contents
        .filter(file => file.name.toUpperCase().match(/(.*\.)(PNG|JPG|JPEG)/));
      const newFiles = images.filter(file => !files.has(file.name));
      let changed = false;
      if (newFiles.length > 0) {
        log.info(
          `Got ${newFiles.length} new files.`
        );
        newFiles.forEach(file => files.set(file.name, file));
        changed = true;
      }
      const contentSet = new Set(images.map(s => s.name));
      const deletedFiles = [...files.keys()].filter(file => !contentSet.has(file));
      if (deletedFiles.length > 0) {
        log.info(
          `${deletedFiles.length} files were removed.`
        );
        deletedFiles.forEach(file => files.delete(file));
        changed = true;
      }
      broadcast(setToJSON(files));
    } else {
      log.warn(`Unable to get images: '${err}'`);
    }
  }, "stat");
}, 5000);

wss.on('connection', (sock: ws) => {
  log.info(`Client connected: ${sock.url}`);
  sock.send(setToJSON(files));
});

server.listen(config.port, () => {
  log.info(
    `Server started on port ${JSON.stringify(server.address())} :)`
  );
});
