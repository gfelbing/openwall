FROM node:8-alpine AS build-env
ADD . /app
WORKDIR /app
RUN yarn install
RUN /app/build.sh

FROM gcr.io/distroless/nodejs
COPY --from=build-env /app/server/dist /app
COPY --from=build-env /app/server/node_modules /app/node_modules
COPY --from=build-env /app/client/dist /app/static
WORKDIR /app
CMD ["index.js"]
